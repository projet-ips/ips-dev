/**
 * @file calc_psi.cpp
 *
 * Fichier implémentant toutes les fonctions de calc_psi.h
 */

#include <iostream>
#include <fstream>
#include "calc_psi.h"

/**
 * Constructeur de la classe Solution, prend en argument n_max et z
 * poiur définir les attributs du même nom de l'instance.
 *
 * @param _n_max entier
 * @param _z vecteur
 */
Solution::Solution(int _n_max, arma::vec _z) {
        n_max = _n_max;
        z = _z;
}

// Privé

/**
 * Fonction utilitaire qui calcule récursivement la factorielle d'un entier.
 *
 * @param n entier dont on souhaite obtenir la factorielle
 * @return entier n!
 */
int Solution::fact(int n) {
        if (n == 0) {
                return 1;
        }

        return (n * fact(n - 1));
}

/**
 * Fonction qui calcule la valeur du n-ième polynôme d'Hermite évalué en z.
 *
 * @param n entier
 * @param z flottant
 * @return flottant Hn(z)
 */
double Solution::Hermite_n_z(int n, double z) {
        if (n == 0) {
                return 1;
        } else if (n == 1) {
                return 2 * z;
        } else {
                return (2 * z * Hermite_n_z(n - 1, z) - 2 * (n - 1) * Hermite_n_z(n - 2, z));
        }
};

/**
 * Fonction qui calcule la solution analytique de l'équation 1D-HO de Schrödinger.
 *
 * @param n entier
 * @param z flottant
 * @return flottant ψn​(z)
 */
double Solution::Calcul_psi(int n, double z) {
        double ra = 1 / (sqrt(pow(2, n) * fact(n)));
        double ex = exp(-(MASS * OMEGA * pow(z, 2)) / (2 * HRED));
        double h = Hermite_n_z(n, sqrt(MASS * OMEGA / HRED) * z);
        double psi = ra * pow((MASS * OMEGA) / (PI * HRED), 1 / 4) * ex * h;

        return psi;
};

/**
 * Fonction qui remplit une colonne de la matrice solMat, attribut de l'instance,
 * avec les valeurs de ψn​(z) pour n fixé et z dans le vecteur du même nom.
 *
 * @param n entier
 */
void Solution::Fill_n(int n) {
        int zSize = z.n_rows;
        arma::vec tmp = arma::vec(zSize);

        for (int i = 0; i < zSize; i++)
        {
                double zVal = z.at(i);
                double psi = Calcul_psi(n, zVal);
                tmp.row(i) = psi;
        }

        solMat.insert_cols(n, tmp);
}

/**
 * Fonction qui estime la dérivée première selon z de la
 * solution analytique de l'équation 1D-HO de Schrödinger ψ,
 * pour n et z donnés.
 *
 * @param n entier
 * @param z flottant
 * @return ψ'n(z)
 */
double Solution::Derive_psi(int n, double z) {
        double ret = Calcul_psi(n, z + DERIVATIVE_STEP) - Calcul_psi(n, z);
        ret /= DERIVATIVE_STEP;
        return ret;
}

/**
 * Fonction qui estime la dérivée seconde selon z de la
 * solution analytique de l'équation 1D-HO de Schrödinger ψ,
 * pour n et z donnés.
 *
 * @param n entier
 * @param z flottant
 * @return ψ''n(z)
 */
double Solution::Seconde_psi(int n, double z) {
        double ret = Derive_psi(n, z + DERIVATIVE_STEP) - Derive_psi(n, z);
        ret /= DERIVATIVE_STEP;
        return ret;
}

/**
 * Dans le cadre de Calcul_integral :
 * Fonction qui calcule ωi(10)
 *
 * @param i entier
 * @return ωi(10)
 */
double Solution::omegai(int i) {
        double res = pow(2, 10-1) * fact(10) * sqrt(PI);
        res /= pow(10, 2) * pow(Hermite_n_z(10 - 1, xi[i]), 2);
        return res;
}

/**
 * Dans le cadre de Calcul_integral :
 * Fonction qui calcule f(z) = Hn(z) * Hm(z)
 *
 * @param n entier
 * @param m entier
 * @param z flottant
 * @return f(z) = Hn(z) * Hm(z)
 */
double Solution::f(int n, int m, double z) {
        return Hermite_n_z(n, z) * Hermite_n_z(m, z);
}

/**
 * Fonction qui calcule l'intégrale présente dans le sujet du projet pour
 * des valeurs de n et m données, afin plus tard de vérifier l'orthonormalité
 * de ψ.
 *
 * @param n entier
 * @param m entier
 * @return flottant ∫ψm∗​(z)ψn​(z)dz
 */
double Solution::Calcul_integral(int n, int m) {
        // La constante sortie de l'intégrale par laquelle il faudra multiplier
        double k = (1 / (pow(2, n + m) * fact(n) * fact(m))) * pow(1 / PI, 1/2);

        double res = 0;
        for (int i = 1; i <= 10; i++) {
                res += omegai(i) * f(n, m, xi[i]);
        }

        res *= k;
        return res;
}

// Public

/**
 * Fonction qui remplit la matrice solMat, attribut de l'instance,
 * avec les valeurs de ψn​(z) pour n entier allant de 0 à n_max inclus
 * et z dans le vecteur du même nom.
 */
void Solution::Fill() {
        for (int i = 0; i <= n_max; i++)
        {
                Fill_n(i);
        }
}

/**
 * Fonction qui exporte dans le fichier src/derivatives.csv des données utiles
 * pour vérifier si les estimations de dérivées via les fonctions
 * Derive_psi et Seconde_psi sont correctes (ne sont pas aberrantes) ;
 * en effet, on peut facilement tracer un graphe depuis ce fichier CSV.
 * En première colonne, les valeurs de z, en deuxième colonne ψn(z),
 * en troisième colonne ψ'n(z) et en dernière colonne ψ''n(z).
 *
 * @param n entier, rang de ψ
 */
void Solution::Export_derivatives(int n) {
        std::ofstream exportCsv;
        exportCsv.open("src/derivatives.csv");

        for (long long unsigned int i = 0; i < z.n_rows; i++) {
                exportCsv << z.at(i) << "," << Calcul_psi(n, z.at(i)) << ",";
                exportCsv << Derive_psi(n, z.at(i)) << "," << Seconde_psi(n, z.at(i));
                exportCsv << std::endl;
        }

        exportCsv.close();
}

/**
 * Fonction qui vérifie si le calcul avec l'hamiltonien donné dans le sujet
 * est bien égal au membre de droite de droite qui fait apparaître
 * l'énergie, en partant de la formule connue des niveaux d'énergie.
 * Affiche le résultat dans le terminal.
 *
 * @param n entier rang de ψ et de E
 */
void Solution::Check_energy(int n) {
        for (long long unsigned int i = 0; i < z.n_rows; i++) {
                double zVal = z.at(i);

                double left = -(pow(HRED, 2) / (2 * MASS)) * Seconde_psi(n, zVal);
                left += (1 / 2) * MASS * pow(OMEGA, 2) * pow(zVal, 2) * Calcul_psi(n, zVal);

                double right = (HRED * OMEGA * (n + 1/2)) * Calcul_psi(n, zVal);

                std::cout << "n = " << n << ", z = " << zVal << std::endl;
                std::cout << left << " = " << right << " ? | diff: " << right - left << std::endl;
        }
}

/**
 * Fonction qui vérifie l'égalité d'orthonormalité de psi donnée dans le sujet
 * avec plusieurs valeurs de n et de m, en affichant le résultat dans le terminal.
 */
void Solution::Check_ortho() {
        int res = 1;
        for(int i = 0; i < n_max + 1; i++) {
                for(int j = 0; j < n_max + 1; j++) {
                        int ortho = Calcul_integral(i, j);
                        if((i == j) & (ortho != 1)) {
                                res = 0;
                        }
                        else if((i != j) & (ortho == 0)) {
                                res = 0;
                        }
                }
        }
        if(res == 1) {
                std::cout << "Les solutions sont bien orthonormales !" << std::endl;
        }
        else {
                std::cout << "Les solutions obtenues ne sont pas orthonormales" << std::endl;
        }
}
