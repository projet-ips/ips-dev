/**
 * @file main.cpp
 *
 * Fichier contenant le code utile à exécuter, puisqu'il contient le point d'entrée à l'exécution
 */

#include "calc_psi.h"
#include <stdio.h>
#include <iostream>
#include <fstream>

using namespace std;
using namespace arma;

/**
 * La fonction main, point d'entrée du code
 */
int main(int argc, char ** argv) {
        colvec z = linspace<colvec>(-5, 5, 200);
        int n_max = 8;
        Solution solution = Solution(n_max, z);
        solution.Fill();

        ofstream exportMat;
        exportMat.open("src/export.txt");

        string zStr = "";
        for (long long unsigned int i = 0; i < z.n_rows; i++) {
                zStr += to_string(z.at(i));
                if (i != z.n_rows - 1)
                        zStr += " ";
        }
        //cout << zStr << endl;
        exportMat << zStr << endl;

        string nStr = "";
        for (int i = 0; i <= n_max; i++) {
                nStr += to_string(i);
                if (i != n_max) nStr += " ";
        }
        //cout << nStr << endl;;
        exportMat << nStr << endl;

        for (int i = 0; i <= n_max; i++) {
                string lineStr = "";
                for (long long unsigned int j = 0; j < z.n_rows; j++) {
                        lineStr += to_string(solution.solMat.at(j, i));
                        if (j != z.n_rows - 1)
                                lineStr += " ";
                }
                //cout << lineStr << endl;
                exportMat << lineStr << endl;
        }

        exportMat.close();

        //solution.Export_derivatives(0);
        //solution.Check_energy(0);
        solution.Check_ortho();

        return 0;
}
