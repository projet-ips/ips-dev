/**
 * @file tests.h
 *
 * Fichier de tests CxxTest des fonctions du projet.
 * Par manque de temps, aucun de ces tests n'est implémenté et ce fichier fait
 * office lui même de fichier de test de CxxTest.
 */

#include "cxxtest/TestSuite.h"

/**
 * Classe de test de la classe Solution.
 */
class TestSolution : public CxxTest::TestSuite
{
public:
/**
 * Fonction de test de CxxTest, peu utile.
 */
void testAddition( void )
{
        TS_ASSERT( 1 + 1 > 1 );
        TS_ASSERT_EQUALS( 1 + 1, 2 );
}
};
