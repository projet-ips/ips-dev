/**
 * @file calc_psi.h
 *
 * Fichier d'interface pour toutes les fonctions utiles du projet, implémentées dans calc_psi.cpp
 */

#ifndef CALC_PSI_H
#define CALC_PSI_H

/// La masse utilisée lors des calculs
#define MASS 1
/// La constante ω utilisée lors des calculs
#define OMEGA 1
/// La constante de Planck réduite ("h barre") utilisée lors des calculs
#define HRED 1
/// La constante mathématique pi
#define PI 3.14159265358979323846
/// Le pas (précision) utilisé pour l'estimation des dérivées
#define DERIVATIVE_STEP 0.001

#include <armadillo>

/**
 * Classe qui contient toutes les fonctions utiles au projet,
 * et qui peut être instanciée pour exporter des données.
 */
class Solution {
private:

/** Racines xi de H10 (on prend p = 10 arbitrairement haut).
 * Trouvées tabulées sur internet.
 * x0 vaut 0 mais n'est pas racine dans ce cas (il est de toutes manières pas utilisé).
 */
double xi[6] = {0, 0.342901327, 1.036610830, 1.756683649, 2.532731674, 3.436159119};

int fact(int);
double Hermite_n_z(int, double);
double Calcul_psi(int, double);
void Fill_n(int);
double Derive_psi(int, double);
double Seconde_psi(int, double);
double f(int, int, double);
double omegai(int);
double Calcul_integral(int, int);

public:
/**
 * Ce vecteur est celui dont on se servira lors de l'export du calcul de psi pour
 * sa représentation, ainsi que lors du check d'énergie, par exemple.
 * On calculera donc psi pour chaque valeur dans z.
 */
arma::vec z;
/**
 * n_max est un entier qui correspond à la valeur maximale de n utilisée lors
 * des calculs, notamment celui de la matrice à exporter issue du calcul de psi,
 * où l'on fera ce calcul pour n allant de 0 à n_max inclus.
 */
int n_max;
/**
 * solMat est la matrice que l'on va remplir de calculs de psi puis exporter
 * pour représenter graphiquement les résultats.
 */
arma::mat solMat = arma::mat();

Solution(int, arma::vec);

void Fill();

void Export_derivatives(int);

void Check_energy(int);

void Check_ortho();
};

#endif
