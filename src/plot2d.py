#!/usr/bin/env python3

import numpy as np
import matplotlib.pyplot as plt
from matplotlib import cm
from mpl_toolkits.mplot3d import Axes3D

# Ouvre le fichier et récupère le contenu
f = open("export.txt", "r")
content = f.read().strip()
lines = content.split("\n")

# Récupère les informations dans le contenu du fichier.
# On s'attend à ce que la première ligne contienne les valeurs prises par z,
# et la deuxième ligne les valeurs prises par n
# Le reste est le tableau des valeurs calculées

z = [float(num) for num in lines[0].split(" ")]
n = [int(num) for num in lines[1].split(" ")]

# Convertit le contenu en tableau (doublement indicé) de flottants
matrix = lines[2:]
for i in range(len(matrix)):
    matrix[i] = [float(num) for num in matrix[i].split(" ")]

# Prépare un plot 3d en surface
fig = plt.figure()
ax = fig.add_subplot(111, projection='3d')
ax.set_xlabel('$z$')
ax.set_ylabel('$n$')

X = np.array(z)
Z = np.matrix(matrix)

# Affiche les plots 2d
for i in n:
    Y = np.asarray(Z)[i]
    ax.plot(X, Y, zdir='y', zs=i)
plt.show()
