CC = g++
CFLAGS = -Wall -std=c++11
CLIB = -larmadillo
DIR = src
OBJ = $(DIR)/calc_psi.o
TARGET = $(DIR)/main
TEST = $(DIR)/tests

all: $(TARGET) clean
test: $(TEST) clean

.PHONY: test all clean

%.o: %.cpp
	g++ $(CFLAGS) -I $(DIR) -c $< -o $@ $(CLIB)

$(TARGET): $(TARGET).o $(OBJ)
	g++ $(CFLAGS) $^ -o $@
	./$(TARGET)

$(TEST).cpp:
	cxxtestgen --error-printer -o $@ $(TEST).h

$(TEST): $(DIR)/tests.o $(OBJ)
	g++ $(CFLAGS) $^ -o $@
	./$(TEST)

clean:
	rm -f $(OBJ)
	rm -f $(TARGET).o $(TARGET)
	rm -f $(TEST).cpp $(TEST).o $(TEST)
