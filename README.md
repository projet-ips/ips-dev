# IPS-DEV

Le premier projet d'IPS, celui d'IPS-DEV (S3).  
Pour générer la documentation, faites la commande `doxygen` dans le dossier principal.  
Pour compiler et exécuter le projet, faites `make` dans ce même dossier.  
Pour compiler et exécuter les tests, faites `make test`.  
La représentation se trouve ici : [pres/html](pres/html).
